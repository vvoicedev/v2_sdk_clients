﻿
using Microsoft.Net.Http.Headers;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamic.Sve.Samples
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string clientId = ""; // "<put client id here. SAMPLE APPLICATION ONLY: client id MUST be the first part of a file name.>";
            string devKey = ""; //"<put dev key here, or type it thru command line>";
            string appKey = ""; // "<put app key here, or type it thru command line>";
            string intId = "";
            string intTag = "";
            WebRequestEnroller.Gender wrGender = WebRequestEnroller.Gender.Unknown;
            HttpClientEnroller.Gender hcGender = HttpClientEnroller.Gender.Unknown;

            if (args.Length >= 1)
                clientId = args[0];

            if (args.Length >= 2)
                devKey = args[1];

            if (args.Length >= 3)
                appKey = args[2];

            if (args.Length >= 4)
            {
                wrGender = args[3].ToLower().Equals('f') ? WebRequestEnroller.Gender.Female : WebRequestEnroller.Gender.Male;
                hcGender = args[3].ToLower().Equals('f') ? HttpClientEnroller.Gender.Female : HttpClientEnroller.Gender.Male;
            }

            if (args.Length >= 5)
                intId = args[4];

            if (args.Length >= 6)
                intTag = args[5];

            new WebRequestEnroller(devKey, appKey, intId, intTag, clientId, wrGender).Enroll();
            new WebRequestVerifier(devKey, appKey, intId, intTag, clientId).Verify();
            
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://demo.v2ondemandapis.com");

            await new HttpClientEnroller(httpClient, devKey, appKey, intId, intTag, clientId, hcGender).Enroll();
            await new HttpClientVerifier(httpClient, devKey, appKey, intId, intTag, clientId).Verify();

            Console.ReadLine();
        }
    }
}
