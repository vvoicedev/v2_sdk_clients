﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;

namespace Dynamic.Sve.Samples
{
    public class HttpClientEnroller
    {
        public enum Gender
        {
            Unknown,
            Male,
            Female
        };

        public enum Mode
        {
            Start,
            Process,
            Finish,
            Cancel
        };

        private readonly HttpClient mHttpClient;
        private readonly string mDevKey;
        private readonly string mAppKey;
        private readonly string mInteractionId;
        private readonly string mInteractionTag;
        private readonly string mClientId;
        private readonly char mGender;
        private string mSessionId;
        private CookieContainer mCookieContainer;

        public HttpClientEnroller(HttpClient httpClient, string devKey, string appKey, string intId, string intTag, string clientId, Gender gender)
        {
            mHttpClient = httpClient;
            mDevKey = devKey;
            mAppKey = appKey;
            mInteractionId = intId;
            mInteractionTag = intTag;
            mClientId = clientId;
            mGender = (gender == Gender.Unknown || gender == Gender.Male) ? 'M' : 'F';
        }

        public async Task Enroll()
        {
            if (await StartEnrollment())
            {
                Console.WriteLine("");
                foreach (string f in Directory.GetFiles("Enroll", $"{mClientId}*.wav"))
                {
                    if (!await AddFileToEnrollment(f))
                    {
                        await CancelEnrollment("failed to enroll");
                        return;
                    }
                    Console.WriteLine("");
                }
                await FinalizeEnrollment();
            }
        }

        async Task<bool> StartEnrollment()
        {
            mCookieContainer = new CookieContainer();
            string uri = $"https://demo.v2ondemandapis.com/1/sve/Enrollment/{mClientId}/{mGender}";
            Console.WriteLine($"Starting Enrollment: {uri}");
            using (var requestMessage = BuildRequest(Mode.Start, uri))
            {
                return await HandleResponse(Mode.Start, requestMessage, await mHttpClient.SendAsync(requestMessage));
            }
        }

        Task<bool> AddFileToEnrollment(string file)
        {
            return UploadFile("https://demo.v2ondemandapis.com/1/sve/Enrollment", new string[] { file });
        }

        async Task<bool> FinalizeEnrollment()
        {
            string uri = "https://demo.v2ondemandapis.com/1/sve/Enrollment";
            Console.WriteLine($"Finalizing Enrollment: {uri}");
            using (var requestMessage = BuildRequest(Mode.Finish, uri))
            {
                return await HandleResponse(Mode.Finish, requestMessage, await mHttpClient.SendAsync(requestMessage));
            }
        }

        async Task<bool> CancelEnrollment(string reason)
        {
            reason = reason.Replace(' ', '-').Substring(0, Math.Min(reason.Length, 64));
            string uri = $"https://demo.v2ondemandapis.com/1/sve/Cancel/{reason}";
            Console.WriteLine($"Canceling Enrollment: {uri}");
            using (var requestMessage = BuildRequest(Mode.Cancel, uri))
            {
                return await HandleResponse(Mode.Cancel, requestMessage, await mHttpClient.SendAsync(requestMessage));
            }
        }

        async Task<bool> UploadFile(string URL, string[] files)
        {
            Console.Write("URL: " + URL + "\n");
            Console.Write("File: " + files[0] + "\n");
            
            Console.WriteLine($"Process Enrollment: {URL}");
            using (var requestMessage = BuildRequest(Mode.Process, URL))
            {
                string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
                var content = new MultipartFormDataContent(boundary);

                for (int i = 0; i < files.Length; i++)
                {
                    Console.WriteLine("Sending Enrollment file: " + URL + " => " + files[i]);

                    FileInfo fileInfo = new FileInfo(files[i]);

                    FileStream fileStream = fileInfo.OpenRead();

                    fileStream.Seek(44, SeekOrigin.Begin); // Skip riff headers

                    content.Add(new StreamContent(fileStream), "data", fileInfo.FullName);
                }

                requestMessage.Content = content;
                return await HandleResponse(Mode.Process, requestMessage, await mHttpClient.SendAsync(requestMessage));
            }
        }

        HttpRequestMessage BuildRequest(Mode mode, string URL)
        {
            HttpRequestMessage request = null;
            switch (mode)
            {
                case Mode.Start:
                    request = new HttpRequestMessage(HttpMethod.Post, URL);
                    request.Headers.Add("Developer-Key", mDevKey);
                    request.Headers.Add("Application-Key", mAppKey);
                    if (!string.IsNullOrEmpty(mInteractionId))
                    {
                        request.Headers.Add("Interaction-Id", mInteractionId);
                    }
                    if (!string.IsNullOrEmpty(mInteractionTag))
                    {
                        request.Headers.Add("Interaction-Tag", mInteractionTag);
                    }
                    break;
                case Mode.Process:
                    request = new HttpRequestMessage(HttpMethod.Post, URL);
                    request.Headers.Add("Vv-Session-Id", mSessionId);
                    break;
                case Mode.Finish:
                case Mode.Cancel:
                    request = new HttpRequestMessage(HttpMethod.Delete, URL);
                    request.Headers.Add("Vv-Session-Id", mSessionId);
                    break;
            }

            request.Version = new Version(1, 0);
            string cookies = mCookieContainer.GetCookieHeader(request.RequestUri);
            if (!string.IsNullOrEmpty(cookies))
            {
                request.Headers.Add("Cookie", cookies);
            }

            return request;
        }

        async Task<bool> HandleResponse(Mode mode, HttpRequestMessage request, HttpResponseMessage response)
        {
            if(response == null)
            {
                Console.WriteLine("Timed-Out");
                return false;
            }

            try
            {
                HttpStatusCode code = response.StatusCode;
                string description = response.ReasonPhrase;

                Console.WriteLine($"Http Status Code: {code}");
                Console.WriteLine($"Http Status Description: {description}");
                
                if (code == HttpStatusCode.OK)
                {
                    if (mode == Mode.Start)
                    {
                        if(!response.Headers.TryGetValues("Vv-Session-Id", out var sessionValue))
                        {
                            Console.WriteLine("Invalid start response, no Vv-Session-Id");
                            return false;
                        }
                        mSessionId = sessionValue.FirstOrDefault();
                        if(string.IsNullOrEmpty(mSessionId))
                        {
                            Console.WriteLine("Invalid start response, empty Vv-Session-Id");
                            return false;
                        }
                    }
                    else if (mode == Mode.Finish || mode == Mode.Cancel)
                    {
                        mSessionId = "";
                    }

                    if (response.Headers.TryGetValues("Set-Cookie", out var newCookies))
                    {
                        foreach (var item in SetCookieHeaderValue.ParseList(newCookies.ToList()))
                        {
                            var uri = new Uri(request.RequestUri, item.Path.Value);
                            mCookieContainer.Add(uri, new Cookie(item.Name.Value, item.Value.Value, item.Path.Value));
                        }
                    }
                }

                if(response.Content == null)
                {
                    Console.WriteLine("Invalid response, no content");
                    return false;
                }

                var content = await response.Content.ReadAsStringAsync();

                if(content == null || content.Length == 0)
                {
                    Console.WriteLine("Invalid response, no content");
                    return false;
                }

                Console.WriteLine("Http Results: " + content);
                
                return code == HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
