﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Dynamic.Sve.Samples
{
    public class WebRequestVerifier
    {
        public enum Mode
        {
            Start,
            Process,
            Finish,
            Cancel
        };

        private readonly string mDevKey;
        private readonly string mAppKey;
        private readonly string mInteractionId;
        private readonly string mInteractionTag;
        private readonly string mClientId;
        private string mSessionId;
        private CookieContainer mCookieContainer;

        public WebRequestVerifier(string devKey, string appKey, string intId, string intTag, string clientId)
        {
            mDevKey = devKey;
            mAppKey = appKey;
            mInteractionId = intId;
            mInteractionTag = intTag;
            mClientId = clientId;
        }

        public void Verify()
        {
            if (StartVerification())
            {
                Console.WriteLine("");
                foreach (string f in Directory.GetFiles("Verify", $"{mClientId}*.wav"))
                {
                    if (!AddFileToVerification(f))
                    {
                        CancelVerification("failed to verify");
                        return;
                    }
                    Console.WriteLine("");
                }
                FinalizeVerification();
            }
        }

        bool StartVerification()
        {
            mCookieContainer = new CookieContainer();

            HttpWebRequest webRequest = BuildRequest(Mode.Start, $"https://demo.v2ondemandapis.com/1/sve/Verification/{mClientId}");

            Console.WriteLine("Starting Verification: " + webRequest.RequestUri);

            return HandleResponse(Mode.Start, webRequest);
        }

        bool AddFileToVerification(string file)
        {
            return UploadFile("https://demo.v2ondemandapis.com/1/sve/Verification", new string[] { file });
        }

        bool FinalizeVerification()
        {
            HttpWebRequest webRequest = BuildRequest(Mode.Finish, "https://demo.v2ondemandapis.com/1/sve/Verification");

            Console.WriteLine("Finalizing Verification: " + webRequest.RequestUri);

            return HandleResponse(Mode.Finish, webRequest);
        }

        bool CancelVerification(string reason)
        {
            reason = reason.Replace(' ', '-').Substring(0, Math.Min(reason.Length, 64));
            HttpWebRequest webRequest = BuildRequest(Mode.Cancel, $"https://demo.v2ondemandapis.com/1/sve/Cancel/{reason}");

            Console.WriteLine("Canceling Verification: " + webRequest.RequestUri);

            return HandleResponse(Mode.Cancel, webRequest);
        }

        bool UploadFile(string URL, string[] files)
        {
            Console.Write("URL: " + URL + "\n");
            Console.Write("File: " + files[0] + "\n");

            HttpWebRequest webRequest = BuildRequest(Mode.Process, URL);
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            webRequest.ContentType = "multipart/form-data; boundary=" + boundary;

            Stream postDataStream = new MemoryStream();

            //adding file data
            byte[] buffer = new byte[1024];
            int bytesRead;

            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine("Sending Verification file: " + webRequest.RequestUri + " => " + files[i]);

                FileInfo fileInfo = new FileInfo(files[i]);
                string fileHeaderTemplate = Environment.NewLine + "--" + boundary + Environment.NewLine + "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"" + Environment.NewLine + "Content-Type: audio/wav" + Environment.NewLine + Environment.NewLine;
                byte[] fileHeaderBytes = Encoding.ASCII.GetBytes(string.Format(fileHeaderTemplate, "data", fileInfo.FullName));
                postDataStream.Write(fileHeaderBytes, 0, fileHeaderBytes.Length);

                FileStream fileStream = fileInfo.OpenRead();

                fileStream.Seek(44, SeekOrigin.Begin); // Skip riff headers

                byte[] bufferFiles = new byte[1024];

                bytesRead = 0;

                while ((bytesRead = fileStream.Read(bufferFiles, 0, bufferFiles.Length)) != 0)
                {
                    postDataStream.Write(bufferFiles, 0, bytesRead);
                }

                fileStream.Close();
            }

            byte[] endBoundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");

            postDataStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);

            webRequest.ContentLength = postDataStream.Length;

            Stream reqStream = webRequest.GetRequestStream();

            postDataStream.Position = 0;

            bytesRead = 0;

            while ((bytesRead = postDataStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                reqStream.Write(buffer, 0, bytesRead);
            }

            postDataStream.Close();
            reqStream.Close();

            return HandleResponse(Mode.Process, webRequest);
        }

        HttpWebRequest BuildRequest(Mode mode, string URL)
        {
            HttpWebRequest webRequest = WebRequest.Create(URL) as HttpWebRequest;
            webRequest.ProtocolVersion = HttpVersion.Version10;
            webRequest.KeepAlive = false;
            webRequest.CookieContainer = mCookieContainer;

            switch (mode)
            {
                case Mode.Start:
                    webRequest.Method = "POST";
                    webRequest.Headers["Developer-Key"] = mDevKey;
                    webRequest.Headers["Application-Key"] = mAppKey;
                    if (!String.IsNullOrEmpty(mInteractionId))
                    {
                        webRequest.Headers["Interaction-Id"] = mInteractionId;
                    }
                    if (!String.IsNullOrEmpty(mInteractionTag))
                    {
                        webRequest.Headers["Interaction-Tag"] = mInteractionTag;
                    }
                    break;
                case Mode.Process:
                    webRequest.Method = "POST";
                    webRequest.Headers["Vv-Session-Id"] = mSessionId;
                    break;
                case Mode.Finish:
                case Mode.Cancel:
                    webRequest.Method = "DELETE";
                    webRequest.Headers["Vv-Session-Id"] = mSessionId;
                    break;
            }

            return webRequest;
        }

        bool HandleResponse(Mode mode, HttpWebRequest webRequest)
        {
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (WebException ex)
            {
                response = (HttpWebResponse)ex.Response;
            }

            if (response == null)
            {
                Console.WriteLine("Timeout Occurred");
                return false;
            }

            HttpStatusCode code = response.StatusCode;
            string description = response.StatusDescription;

            Console.WriteLine($"Http Status Code: {code}");
            Console.WriteLine($"Http Status Description: {description}");

            bool rc = code == HttpStatusCode.OK;

            if (mode == Mode.Start)
            {
                mSessionId = response.Headers["Vv-Session-Id"];
            }
            else if (mode == Mode.Finish || mode == Mode.Cancel)
            {
                mSessionId = "";
                mCookieContainer = null;
            }

            if (response.ContentLength > 0)
            {
                try
                {
                    Stream stream = response.GetResponseStream();
                    if (stream != null)
                    {
                        Console.WriteLine("Http Results: " + (new StreamReader(stream)).ReadToEnd());
                    }
                    else
                    {
                        Console.WriteLine("Http Results: No Content");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    rc = false;
                }
            }

            return rc;
        }
    }
}
